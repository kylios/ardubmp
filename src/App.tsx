import React from 'react'
import './App.css'

function readFromSprite(
  sprite: Array<Array<boolean>>,
  row: number,
  col: number,
) {
  if (sprite.length <= row) {
    return false
  }
  if (sprite[row].length <= col) {
    return false
  }
  return sprite[row][col]
}

type fn = () => void
interface CellProps {
  value: boolean
  onClick: fn
}

function Cell({ value, onClick }: CellProps) {
  const classnames = ['cell']
  if (value) {
    classnames.push('toggled')
  }
  return <div onClick={onClick} className={classnames.join(' ')} />
}

type toggleCellFn = (row: number, col: number) => void
interface EditorProps {
  width: number
  height: number
  sprite: Array<Array<boolean>>
  toggleCell: toggleCellFn
}

function Editor({ width, height, sprite, toggleCell }: EditorProps) {
  const rows = []
  for (let r = 0; r < height; r++) {
    const row = []
    for (let c = 0; c < width; c++) {
      row.push(
        <Cell
          key={`${r}_${c}`}
          value={readFromSprite(sprite, r, c)}
          onClick={() => toggleCell(r, c)}
        />,
      )
    }
    rows.push(
      <div key={r} className='row'>
        {row}
      </div>,
    )
  }
  return <div className='editor'>{rows}</div>
}

interface CodeDisplayProps {
  sprite: Array<Array<boolean>>
}
function CodeDisplay({ sprite }: CodeDisplayProps) {
  const width = sprite.length ? sprite[0].length : 0
  const height = sprite.length

  const array = React.useMemo(() => {
    const array: Array<string> = []
    for (let col = 0; col < width; col++) {
      let byte = 0
      for (let row = 0; row < height; row++) {
        const value = sprite[row][col] ? 1 : 0
        byte += value * Math.pow(2, row)
      }
      array.push(`0x${byte.toString(16)}`)
    }
    return array
  }, [sprite, width, height])

  const code = `const unsigned char PROGMEM sprite[] = {
    ${width}, ${height},
    ${array.join(', ')}
  };`
  return <div className='code_display'>{code}</div>
}

function App() {
  const [width, setWidth] = React.useState(8)
  const [height, setHeight] = React.useState(8)
  const [sprite, setSprite] = React.useState<Array<Array<boolean>>>([])

  const toggleCell = (row: number, col: number) => {
    // Create a deep copy
    const newSprite = sprite.map((row) => row.slice())
    newSprite[row][col] = !newSprite[row][col]
    setSprite(newSprite)
  }

  const toggleAllCells = () => {
    const newSprite = sprite.map(row => row.map(col => !col))
    setSprite(newSprite)
  }

  const resetCells = () => {
    const newSprite = []
    for (let r = 0; r < height; r++) {
      const row = []
      for (let c = 0; c < width; c++) {
        row.push(false)
      }
      newSprite.push(row)
    }
    setSprite(newSprite)
  }

  React.useEffect(() => {
    resetCells()
  }, [height, width])

  return (
    <div className='App'>
      <div className='controls'>
        <input
          type='text'
          value={width}
          onChange={(e) =>
            setWidth(!!e.target.value ? parseInt(e.target.value) : 0)
          }
        />
        <input
          type='text'
          value={height}
          onChange={(e) =>
            setHeight(!!e.target.value ? parseInt(e.target.value) : 0)
          }
        />
        <input type={'button'} onClick={toggleAllCells} value={'Reverse'} />
        <input type={'button'} onClick={resetCells} value={'Reset'} />
      </div>
      <Editor
        width={width}
        height={height}
        sprite={sprite}
        toggleCell={toggleCell}
      />
      <CodeDisplay sprite={sprite} />
    </div>
  )
}

export default App
